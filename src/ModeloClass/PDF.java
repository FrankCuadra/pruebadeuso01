/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

/**
 *
 * @author FRANCISCO
 */
public class PDF {
    /*Atributos*/
     int Codigo_PDF;
     String Nombre_PDF;
     byte[] Archivo_PDF;
     int ID;
     int ID_U;
     
     /*Getter y Setter*/

    public int getCodigo_PDF() {
        return Codigo_PDF;
    }

    public void setCodigo_PDF(int Codigo_PDF) {
        this.Codigo_PDF = Codigo_PDF;
    }

    public String getNombre_PDF() {
        return Nombre_PDF;
    }

    public void setNombre_PDF(String Nombre_PDF) {
        this.Nombre_PDF = Nombre_PDF;
    }

    public byte[] getArchivo_PDF() {
        return Archivo_PDF;
    }

    public void setArchivo_PDF(byte[] Archivo_PDF) {
        this.Archivo_PDF = Archivo_PDF;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_U() {
        return ID_U;
    }

    public void setID_U(int ID_U) {
        this.ID_U = ID_U;
    }
     
    
    
}
