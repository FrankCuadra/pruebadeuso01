/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author FRANCISCO
 */
public class SqlPago extends ConexionPostgre {
    
        public boolean Registrar(User scs )
    {
   
        try {
            String SQLQuery = " INSERT INTO public.\"pagos\"(\n" +
"       \"nombre\", \"apellido\", \"empresa\", \"direccion\", \"numero_de_tarjeta\", \"cod_de_seg\", \"venci\", \"plan\")\n" +
"	VALUES (?, ?, ?, ?, ?, ?, ?,?); ";
            Connection conexion = ConexionBD();
            PreparedStatement Pst = conexion.prepareStatement(SQLQuery);
           
           // Pst.setString(1, scs.getid_clie());
            Pst.setString(1, scs.getnombre());
            Pst.setString(2, scs.getapellido());
            Pst.setString(3, scs.getmpresa());
            Pst.setString(4, scs.getdireccion());
            Pst.setString(5, scs.getnumero_de_tarjeta());
            Pst.setString(6, scs.getcod_de_seg());
            Pst.setString(7, scs.getvenci());
            Pst.setString(8, String.valueOf(scs.getplan()));
            
           
            Pst.executeUpdate();
           // JOptionPane.showMessageDialog(null, "Registro Exitoso");
            return true;
            
            
        } catch (SQLException sQLException) {
            JOptionPane.showMessageDialog(null, sQLException);
           // JOptionPane.showMessageDialog(null, "No se Pudo Registar");
        }
        return false;
        
        
    }
        
        
    
}
