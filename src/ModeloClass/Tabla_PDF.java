package ModeloClass;



import java.awt.Image;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Tabla_PDF {

    SqlPDF dao = null;

    public void Visualizar_PDF(JTable tabla) {
        tabla.setDefaultRenderer(Object.class, new ImgTabla());
        DefaultTableModel dt = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dt.addColumn("Codigo del PDF");
        dt.addColumn("Nombre del PDF");
        dt.addColumn("Archivo PDF");
        
        ImageIcon icono = null;
        if (get_Image("/Imagenes/PDF01.png") != null) {
            icono = new ImageIcon(get_Image("/Imagenes/PDF01.png"));
        }

        dao = new SqlPDF();
        PDF vo = new PDF();
        
        ArrayList<PDF> list = dao.Listar_PDF();
   
       if (list.size() > 0) {
           for (int i = 0; i < list.size(); i++) {
               Object fila[] = new Object[3];
               vo = list.get(i);
               fila[0] = vo.getCodigo_PDF();
               fila[1] = vo.getNombre_PDF();
               if (vo.getArchivo_PDF()!= null) {
                   fila[2] = new JButton(icono);
               } else {
                   fila[2] = new JButton("Vacio");
               }
               
               dt.addRow(fila);
           }
           tabla.setModel(dt);
           tabla.setRowHeight(32);
       }
       
   
    }

    public Image get_Image(String ruta) {
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource(ruta));
            Image mainIcon = imageIcon.getImage();
            return mainIcon;
        } catch (Exception e) {
        }
        return null;
    }
}
