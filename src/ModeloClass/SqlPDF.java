/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author FRANCISCO
 */
public class SqlPDF extends ConexionPostgre {
    
     /*Metodo listar*/
    public ArrayList<PDF> Listar_PDF() {
        ArrayList<PDF> list = new ArrayList<PDF>();
        Connection conexion = ConexionBD();
        String sql = "SELECT \"Codigo_PDF\", \"Nombre_PDF\", \"Archivo_PDF\"\n" +
"	FROM public.\"Archivos\";";
        ResultSet rs = null;
        PreparedStatement ps = null;
        
        try {
            ps = conexion.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                PDF vo = new PDF();
                vo.setCodigo_PDF(rs.getInt(1));
                vo.setNombre_PDF(rs.getString(2));
                vo.setArchivo_PDF(rs.getBytes(3));
               // vo.setID(rs.getInt(4));
               // vo.setID_U(rs.getInt(5));
                list.add(vo);
                }
                
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
                DesconexionBD();
            } catch (Exception ex) {
            }
        }
        return list;
    }
    
        /*Metodo agregar*/
    public void Agregar_PDF(PDF vo) {
        Connection conexion = ConexionBD();
        String sql = "INSERT INTO public.\"Archivos\"(\n" +
"       \"Nombre_PDF\", \"Archivo_PDF\" , \"ID\")\n" +
"	VALUES (?, ?, ?);";
        PreparedStatement ps = null;
        try {
            ps = conexion.prepareStatement(sql);
            //ps.setInt(1, vo.getCodigopdf());
            ps.setString(1, vo.getNombre_PDF());
            ps.setBytes(2, vo.getArchivo_PDF());
            ps.setInt(3, vo.getID());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                DesconexionBD();
            } catch (Exception ex) {
            }
        }
    }
    
     /*Metodo Modificar*/
    public void Modificar_PDF(PDF vo) {
        Connection conexion = ConexionBD();
        String sql = "UPDATE public.\"Archivos\"\n" +
"	SET \"Nombre_PDF\"=?, \"Archivo_PDF\"=?\n" +
"	WHERE \"Codigo_PDF\"=?;";
        PreparedStatement ps = null;
        try {
            ps = conexion.prepareStatement(sql);
            ps.setString(1, vo.getNombre_PDF());
            ps.setBytes(2, vo.getArchivo_PDF());
            ps.setInt(3, vo.getCodigo_PDF());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                DesconexionBD();
            } catch (Exception ex) {
            }
        }
    }
    
    /*Metodo Modificar 2*/
    public void Modificar_PDF2(PDF vo) {
        Connection conexion = ConexionBD();
        String sql = "UPDATE public.\"Archivos\"\n" +
"	SET \"Nombre_PDF\"=?\n" +
"	WHERE \"Codigo_PDF\"=?;";
        PreparedStatement ps = null;
        try {
            ps = conexion.prepareStatement(sql);
            ps.setString(1, vo.getNombre_PDF());
            ps.setInt(2, vo.getCodigo_PDF());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                DesconexionBD();
            } catch (Exception ex) {
            }
        }
    }
    
     /*Metodo Eliminar*/
    public void Eliminar_PDF(PDF vo) {
        Connection conexion = ConexionBD();
        String sql = "DELETE FROM public.\"Archivos\"\n" +
"	WHERE \"Codigo_PDF\"=?";
        PreparedStatement ps = null;
        try {
            ps = conexion.prepareStatement(sql);
            ps.setInt(1, vo.getCodigo_PDF());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                DesconexionBD();
            } catch (Exception ex) {
            }
        }
    }
    
    //Permite mostrar PDF contenido en la base de datos
    public void ejecutar_archivoPDF(int id) {

        Connection conexion = ConexionBD();
        PreparedStatement ps = null;
        ResultSet rs = null;
        byte[] b = null;

        try {
            ps = conexion.prepareStatement("SELECT \"Archivo_PDF\"\n" +
"	FROM public.\"Archivos\" WHERE \"Codigo_PDF\"= ?;");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                b = rs.getBytes(1);
            }
            InputStream bos = new ByteArrayInputStream(b);

            int tamanoInput = bos.available();
            byte[] datosPDF = new byte[tamanoInput];
            bos.read(datosPDF, 0, tamanoInput);

            OutputStream out = new FileOutputStream("new.pdf");
            out.write(datosPDF);

            //abrir archivo
            out.close();
            bos.close();
            ps.close();
            rs.close();
            DesconexionBD();

        } catch (IOException | NumberFormatException | SQLException ex) {
            System.out.println("Error al abrir archivo PDF " + ex.getMessage());
        }
    }

    
}
