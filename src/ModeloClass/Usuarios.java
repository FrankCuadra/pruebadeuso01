/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

/**
 *
 * @author FRANCISCO
 */
public class Usuarios {
    
    private int ID;
    private String NombreUsr;
    private String Nombres;
    private String Apellidos;
    private String Telefono;
    private String Paquete;
    private String Empresa;
    private String Contraseña;
    private String Ulsesion;
    private String PaqueteT;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombreUsr() {
        return NombreUsr;
    }

    public void setNombreUsr(String NombreUsr) {
        this.NombreUsr = NombreUsr;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getPaquete() {
        return Paquete;
    }

    public void setPaquete(String Paquete) {
        this.Paquete = Paquete;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String Empresa) {
        this.Empresa = Empresa;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

    public String getUlsesion() {
        return Ulsesion;
    }

    public void setUlsesion(String Ulsesion) {
        this.Ulsesion = Ulsesion;
    }

    public String getPaqueteT() {
        return PaqueteT;
    }

    public void setPaqueteT(String PaqueteT) {
        this.PaqueteT = PaqueteT;
    }
    
    
}
