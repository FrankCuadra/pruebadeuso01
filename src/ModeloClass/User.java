/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloClass;

/**
 *
 * @author CASTELLON-PC y Francisco
 */
public class User {
    private int id_clie;
    private String nombre;
    private String apellido;
    private String empresa;
    private String direccion;
    private String numero_de_tarjeta;
    private String cod_de_seg;
    private String venci;
    private String plan;
  
 /*   
    public int getid_clie() {
        return id_clie;
    }

    public void setid_clie(int id_clie) {
        this.id_clie = id_clie;
    }
*/
    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }


     public String getapellido() {
        return apellido;
     }
    public void setapellido(String apellido) {
        this.apellido = apellido;
    }

    public String getdireccion() {
        return direccion;
    }

    public void setdireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getnumero_de_tarjeta() {
        return numero_de_tarjeta;
    }

    public void setnumero_de_tarjeta(String numero_de_tarjeta) {
        this.numero_de_tarjeta = numero_de_tarjeta;
    }

    public String getcod_de_seg() {
        return cod_de_seg;
    }

    public void setcod_de_seg(String cod_de_seg) {
        this.cod_de_seg =cod_de_seg;
    }

   

    public String getvenci() {
        return venci;
    }

    public void setvenci(String venci) {
        this.venci = venci;
    }
    
     public String getmpresa() {
        return empresa;
    }

    public void setempresa(String empresa) {
        this.empresa = empresa;
    }
    
    public String getplan() {
        return plan;
    }

    public void setplan(String plan) {
        this.plan = plan;
    }
}
